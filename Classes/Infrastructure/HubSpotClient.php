<?php declare(strict_types=1);

namespace Jl\HubSpot\Infrastructure;

use GuzzleHttp\Client;

class HubSpotClient
{
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function get()
    {

    }

    public function post()
    {

    }
}
