<?php declare(strict_types=1);

namespace Jl\HubSpot\Domain\Marketing;

use GuzzleHttp\Client;
use Neos\Flow\Annotations as Flow;

/**
 * Class FormService
 * @package Jl\HubSpot\Domain\Marketing
 * @Flow\Scope("singleton")
 */
class FormService
{
    /**
     * @Flow\InjectConfiguration(package="Jl.HubSpot", path="api.key")
     * @var string
     */
    protected $apiKey;

    public function getAllForms()
    {
        $client = new Client();
        $response = json_decode(
            $client->get('https://api.hubapi.com/marketing/v3/forms/?hapikey=' . $this->apiKey)
                ->getBody()
                ->getContents(),
            true
        );

        $results = $response['results'];

        if (!$results) {
            return [];
        }

        foreach ($results as $result) {
            $forms[] = Form::fromApi($result);
        }

        return $forms;
    }
}
